/**
 * File provides a shell for implementing publisher and subscriber
 * functionality in a single node.  The 
 * in
 */
#include <cmath>
#include <vector>
using namespace std;

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "sensor_msgs/LaserScan.h"


// NOTE: Path might need to be changed if package not named cs4313_launch
#include "robotUtilities.h"
#include "searchUtilities.h"



/** Global variable that will provide callback access to robot state */
RobotState state;
bool targetFound = false;
//States
bool bigTurn = true;
bool atWayPoint = false;
bool moveToPoint = true;
bool startAvoidance = false;
bool wallFollow = false;
bool wallAvoid = false;
bool notMoving = false;
bool wallPerp = false;
bool saveState[8] = {bigTurn, atWayPoint, moveToPoint, startAvoidance, wallFollow, wallAvoid, notMoving, wallPerp};
double saveX = 0;
double saveY = 0;
double saveTHETA = 0;


int possibleTargetFound = 0;
double corrector = 4.0;
double minDistance = 1000;
double maxDistance = 0;
int numDistSame = 0;

bool quiet = false;

double lastx =0;
double lasty =0;
double lasttheta = 0;
int numAllSame = 0;
double coordx;
double coordy;
int numTarget = 0;
double TargetLocsX [10] = {-11.1, -9.7, -4.6, -6.1, -1, 0.4, 5.3, 4.2, 10, 10};
double TargetLocsY [10] = { -7, 5.4, 7, -7, -6.8, 6.4, 5.2, -4.8, -5.2, 4};


/**
 * Callback for receipt of new odometry from the create.  This method will
 * parse state information from the nav_msgs/Odometry message and assign
 * it to the global state variable.
 */
void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
//    state.pose[X] = msg->pose.pose.position.x;
//    state.pose[Y] = msg->pose.pose.position.y;
//    state.pose[THETA] = yawFromQuaternion(msg->pose.pose.orientation);
    state.velocity[X] = msg->twist.twist.linear.x;
    state.velocity[Y] = msg->twist.twist.linear.y;
    state.velocity[THETA] = msg->twist.twist.angular.z;

//    ROS_INFO("Position: (%6.2f, %6.2f, %5.2f), Velocity: (%6.2f, %6.2f, %5.2f)",
//        state.pose[X], state.pose[Y], state.pose[THETA],
//        state.velocity[X], state.velocity[Y], state.velocity[THETA]);
} // odomCallback


void amclPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg) 
{
    state.pose[X] = msg->pose.pose.position.x;
    state.pose[Y] = msg->pose.pose.position.y;
    state.pose[THETA] = yawFromQuaternion(msg->pose.pose.orientation);
 	ROS_INFO("Position: (%6.2f, %6.2f, %5.2f)",
        state.pose[X], state.pose[Y], state.pose[THETA]);
} // amclPoseCallback


/**
 * Callback for receipt of new laser data from the Hokuyo (or other) laser.
 * This method will parse state information from the sensor_msgs/LaserScan
 * message and assign it to the global robot state variable.
 */
void laserCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
    state.laserMinAngle = msg->angle_min;
    state.laserMaxAngle = msg->angle_max;
    state.laserAngleRes = msg->angle_increment;
    state.laserNumReturns = msg->ranges.size();
    state.laserMinRange = msg->range_min;
    state.laserMaxRange = msg->range_max;

    // Get the actual ranges from the message, but set anything greater than or
    // equal to maxRange or less than minRange to 0
    for (int rtn = 0; rtn < state.laserNumReturns; rtn++)
    {
        state.laserRanges[rtn] = msg->ranges[rtn];
        if ((state.laserRanges[rtn] >= state.laserMaxRange) ||
            (state.laserRanges[rtn] < state.laserMinRange))
            state.laserRanges[rtn] = 0.0;
    } // for (int rtn = 0;...
} // laserCallback

/**
 * Callback for receipt of target location and characteristics
 *  - (Point.x, Point.y) = center coordinates of the detected circle
 *  - (Point.z) = radius (in meters) of the detected circle
 */
void circleCallback(const geometry_msgs::Point::ConstPtr& msg)
{
    // Define local variable for circle parameter
    double radius = msg->z;

    // Check parameters for valid target
    double target_diam = 0.50;                // Known target diameter
    double threshold = 0.1 * target_diam;     // The diameter of the circle must be within 10% of the known target diameter

    if ( abs(2*radius-target_diam )<= threshold )  // If this target is within threshold
    {
	if (possibleTargetFound > 15){
        targetFound = true;
        printf("*****************************************\n");
        printf("*****************************************\n");
        printf("Target found at (%2.4f,%2.4f) with radius %2.4fm\n", msg->x, msg->y, radius);}
	possibleTargetFound++;
   } possibleTargetFound = 0;


} // circleCallback


/**
 * Function should use the robot state information and decide how to command
 * the vehicle.  For now the function does not make any computations and 
 * simply publishes a fixed velocity to the "cmd_vel" topic.  These values
 * can be computed instead based on what the current state of the vehicle is
 * and we want it to do
 */

//////My code
///////////////////////////
///////////////////////////

struct Heading
{
	double linx;
	double angz;
};

//int numStop =0;

bool stuck(){
	double newError = lastx/state.pose[X] + lasty/state.pose[Y] + lasttheta/state.pose[THETA];
	if ( newError > 2.90 &&  newError < 3.10){
		numAllSame++;
		if (numAllSame == 1){
			lastx = state.pose[X];
			lasty = state.pose[Y];
			lasttheta = state.pose[THETA];
		}
	}else{
		numAllSame = 0;
		lastx = state.pose[X];
		lasty = state.pose[Y];
		lasttheta = state.pose[THETA];
	}
	

	if (numAllSame > 30){
		numAllSame = 0;
		return true;
	}else {

		return false;
	}
}

bool wallFront(){
int numHits = 0;
int midNum = 0;
	if (state.laserNumReturns % 2 == 0){
		midNum = state.laserNumReturns / 2;
	} else {
		midNum = (state.laserNumReturns + 1)/2;
	}
	for (int n = 1; n<11; n++){
		if (state.laserRanges[midNum+n] > 0 && state.laserRanges[midNum+n] < 2){
			numHits++;
		}
		if (state.laserRanges[midNum-n] > 0 && state.laserRanges[midNum-n] < 2){
			numHits++;
		}
	}
	if (numHits > 2){
	return true;
	}else{
	return false;
	}
}

bool wallTooClose(){
int numHits = 0;
int midNum = 0;
	if (state.laserNumReturns % 2 == 0){
		midNum = state.laserNumReturns / 2;
	} else {
		midNum = (state.laserNumReturns + 1)/2;
	}
	for (int n = 1; n<40; n++){
		if (state.laserRanges[midNum+n] > 0 && state.laserRanges[midNum+n] < 0.25){
			numHits++;
		}
		if (state.laserRanges[midNum-n] > 0 && state.laserRanges[midNum-n] < 0.25){
			numHits++;
		}
	}
	if (numHits > 2){
	return true;
	}else{
	return false;
	}
}

Heading followWall(){
double x1 = 0.0;
double y1 = 0.0;
double x2 = 0.0;
double y2 = 0.0;
double initd = 0.0;
int point1 = 0;
int point2 = 0;
int start = 0;
int midNum = 0;
Heading newHeading;
	if (state.laserNumReturns % 2 == 0){
		midNum = state.laserNumReturns / 2;
	} else {
		midNum = (state.laserNumReturns + 1)/2;
	}


if (wallFront()){
start = midNum;
}

for (int n = start; n< state.laserNumReturns; n++){
	if (state.laserRanges[n] > 0){
		point1 = n;
		n = state.laserNumReturns;
	}
	if (n == state.laserNumReturns - 1){
		newHeading.linx = .40;
		newHeading.angz = -0.5;
		return newHeading;
	}
}
for (int n = point1 + 1; n< state.laserNumReturns; n++){
	if (state.laserRanges[n]/state.laserRanges[n-1] < .90 || state.laserRanges[n]/state.laserRanges[n-1] > 1.10){
		point2 = n;
		n = state.laserNumReturns;
	}
	if (n == state.laserNumReturns - 1){
		point2 = n;
	}
}
if (quiet){
	cerr << "point1 " << point1 << ", point2: " << point2 << endl;
}
 
x1 = cos(state.laserMinAngle + point1 * state.laserAngleRes) * state.laserRanges[point1];
y1 = sin(state.laserMinAngle + point1 * state.laserAngleRes) * state.laserRanges[point1];

x2 = cos(state.laserMinAngle + point2 * state.laserAngleRes) * state.laserRanges[point2];
y2 = sin(state.laserMinAngle + point2 * state.laserAngleRes) * state.laserRanges[point2];

initd = sqrt(pow(y2-y1,2) + pow(x2-x1,2));

y2 = 20*(y2-y1) + y1;
x2 = (20*(x2-x1) + x1);

if (quiet){
	cerr << "old x2: " << x2 << endl;
	cerr << "mod x2: " << x2 << endl;
}

newHeading.linx = saturate(2.0 * initd, 0.1, 1.0);
newHeading.angz = saturate(normalizePI(atan2(y2,x2)), 0.75);
if (quiet){
cerr << "x2: " << x2 << ", y2: " << newHeading.angz << endl;
}
return newHeading;

}

Heading calcMovement(double x, double y){  //x and y cood of target
	Heading newHeading;
	newHeading.linx = 0.0;
	newHeading.angz = 0.0;
	double xcomp = 0;
	double ycomp = 0;

	for (int n = 1; n< state.laserNumReturns; n++){
		
		if (state.laserRanges[n] > 0 && state.laserRanges[n] < 3.0){
			if (abs(state.laserRanges[n] - state.laserRanges[n-1]) < .1){
				xcomp = xcomp - cos(normalize2PI(state.laserMinAngle + float(n) * state.laserAngleRes))/ (pow(state.laserRanges[n],3) * state.laserNumReturns);// * (1.01 - float(abs(midNum - n))/float(midNum)) ;//(state.laserNumReturns * state.laserRanges[midNum+n]);
				ycomp = ycomp - sin(normalize2PI(state.laserMinAngle + float(n) * state.laserAngleRes))/ (pow(state.laserRanges[n],3) * state.laserNumReturns);// * (1.01 - float(abs(midNum - n))/float(midNum)) ;// (state.laserNumReturns * state.laserRanges[midNum+n]);
			}
		}
	}
		
	//cerr << "xcomp: " << xcomp << ", ycomp: " << ycomp << endl;
	xcomp = xcomp + 4* cos(atan2((y-state.pose[Y]),(x-state.pose[X]))- state.pose[THETA])/pow(distance(state.pose[X],state.pose[Y], x, y),1);
	ycomp = ycomp + 4* sin(atan2((y-state.pose[Y]),(x-state.pose[X]))- state.pose[THETA])/pow(distance(state.pose[X],state.pose[Y], x, y),1);
	//cerr << "new xcomp: " << xcomp << ", new ycomp: " << ycomp << endl;
	
	newHeading.angz = normalizePI(atan2(ycomp, xcomp));
	if (newHeading.angz > 0){
	newHeading.linx = 2 * (M_PI - newHeading.angz)/M_PI ;
	}else{
	newHeading.linx = 2 * (M_PI + newHeading.angz)/M_PI ;
	}
	
	//cerr << "calcMovement, linx: " << newHeading.linx << ", angz: " << newHeading.angz << endl;
	return newHeading;
}


/////////////////////////////////
/////////////////////////////////
/////////////////////////////////
/////////////////////////////////





void sendCommand(ros::Publisher& pub)
{
    // Container for the command that will be published
    geometry_msgs::Twist command;

coordx = TargetLocsX[numTarget];
coordy = TargetLocsY[numTarget];

Heading whereTo;

double d = distance(state.pose[X],state.pose[Y], coordx, coordy);
double thetaCMD = atan2((coordy-state.pose[Y]),(coordx-state.pose[X]));
double thetaError = normalizePI(thetaCMD - state.pose[THETA]);


    if (targetFound)
    {
      // For now commanding fwd speed of 0.2 m/s and rt turn of 0.2 rad/sec
      command.linear.x = 0.0;
      command.angular.z = 0.0;
    }
    else
    {
if (quiet){     
cerr << "X-pos " << state.pose[X] << ", Y-pos " << state.pose[Y] << endl;
}
if (stuck()){
	notMoving = true;
}

if (notMoving){
	if (numDistSame++ > 6){
		notMoving = false;
		numDistSame = 0;
		command.angular.z = 0.0;
		command.linear.x = 0.0;
	} else{
		command.angular.z = 0.75;
		command.linear.x = -1.0;
	}
	notMoving = false;
	startAvoidance = false;
	moveToPoint = true;
	pub.publish(command);
	return;
}

if (bigTurn){
	if (thetaError > 0.5 || thetaError < -0.5){
		command.angular.z = saturate(thetaError, 1.5);
		command.linear.x = 0.0;
		cerr << "Turning, thetaError: " << thetaError << ", Command: " << command.angular.z << endl;
		cerr << "xcomp: " << state.laserMinRange << ", ycomp: " << state.laserNumReturns << endl;
 	}else{
		command.angular.z = 0.0;
		command.linear.x = 0.0;
		bigTurn = false;}
	pub.publish(command);
	return;
}
if (moveToPoint){
	if (d < .5){
		command.linear.x = 0.0;
		command.angular.z = 0.0;
		pub.publish(command);
		atWayPoint = true;
		moveToPoint = false;
		return;
	}
	if ((thetaError > 0.5 || thetaError < -0.5) && d > .5){
		command.angular.z = saturate(thetaError, 1.0);
		command.linear.x = 0.1;
	}else{
		command.linear.x = saturate(2.0 * d, 0.0, 2.0);
		command.angular.z = saturate(thetaError, 0.5);
	}
	
	if (wallFront()){
		cerr << "WALL!!!!!!!!!!!!!!!!" << endl;
		moveToPoint = false;
		startAvoidance = true;
		//wallFollow = true;		
		command.linear.x = 0.0;
		command.angular.z = 0.0;
	}
	pub.publish(command);
	return;
	
}

if (wallAvoid){
	cerr << "WALL TOO CLOSE!!!!!!!!!!!!!!!!" << endl;
	if (wallTooClose()){
		command.linear.x = 0.0;
		command.angular.z = 0.5;
		
	}else{
		startAvoidance = true;
		wallAvoid = false;
	}
	pub.publish(command);
	return;
}

if (wallPerp){
	if (state.laserRanges[50] > 1.1 || state.laserRanges[50] < 0.90){
		command.linear.x = 0.0;
		command.angular.z = 0.5;
		pub.publish(command);
		return;
	}else{
		wallPerp = false;
		wallFollow = true;
	}
}

if (wallFollow){
	cerr << "Wall Follow" << endl;
	
	whereTo = followWall();
	
	command.linear.x = whereTo.linx;
	command.angular.z = whereTo.angz;
	
	if (wallTooClose()){
		cerr << "Wall too close" << endl;
		command.linear.x = 0.0;
	}
	pub.publish(command);
	return;
}

if (atWayPoint){
	atWayPoint = false;
	moveToPoint = true;
	numTarget++;	
}

if (startAvoidance){
	double maxSpeed = 1.5;
	if (wallTooClose()){
		startAvoidance = false;
		wallAvoid = true;
		command.linear.x = 0.0;
		command.angular.z = 0.0;
		pub.publish(command);
		return;
	}
	if (normalizePI(thetaError - M_PI) < -2.75 && normalizePI(thetaError - M_PI) > 2.75){
		startAvoidance = false;
		wallFollow = true;
		command.linear.x = 0.0;
		command.angular.z = 0.0;
		pub.publish(command);
		return;
	}

	if (d < .5){
		command.linear.x = 0.0;
		command.angular.z = 0.0;
		pub.publish(command);
		startAvoidance = false;
		atWayPoint = true;
		return;
	}
	
	bool minMaxChange = false;
	if (minDistance > d){
	minDistance = d;
	minMaxChange = true;
	}
	if (maxDistance < d){
	maxDistance = d;
	minMaxChange = true;
	}
	if (minMaxChange){
	numDistSame = 0;
	cerr << "numDist back to zero" << endl;
	}else {numDistSame++;}

	if (numDistSame > 100){
	//wallFollow = true;
	numTarget++;
	startAvoidance = false;
	bigTurn = true;
	command.linear.x = 0.0;
	command.angular.z = 0.0;
	pub.publish(command);
	maxDistance = 0;
	minDistance = 1000;
	return;
	}
	

	whereTo = calcMovement(coordx, coordy);
	if (wallFront()){
	maxSpeed = 0.5;
	}
	command.linear.x = saturate(whereTo.linx, 0.3, maxSpeed);
	command.angular.z = saturate(whereTo.angz, 0.75);
	pub.publish(command);
	return;
}
    }

    pub.publish(command);
} // sendCommand


/**** Main ****/
int main(int argc, char **argv)
{
    ros::init(argc, argv, "simplePubSub_node");
    ros::NodeHandle node;

    // Subscription objects that will listen to the "odom" and "scan" topics
    ros::Subscriber odomSub = node.subscribe("odom", 1, odomCallback);
    ros::Subscriber FakeLoc = node.subscribe("amcl_pose", 1, amclPoseCallback);
    //ros::Subscriber laserSub = node.subscribe("scan", 1, laserCallback);
	
    ros::Subscriber laserSub = node.subscribe("base_scan/scan", 1, laserCallback);
    ros::Subscriber circleSub = node.subscribe("circle_detect", 1, circleCallback);

    // Publisher object that will publish commands to the "cmd_vel" topic
    ros::Publisher velPub = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);

    ros::Rate timer(10); // set control loop to run at 10 hz

    while (ros::ok())   // can modify to have some "mission complete" criteria later
    {
        ros::spinOnce(); // Process all pending callbacks
        sendCommand(velPub);  // Decide what to do and publish commands

        if (targetFound)
        {
            printf("*****************************************\n");
            printf("*****************************************\n");
            printf("Target found!!!\n");
            cout << "Press the ENTER key to exit";
            if (cin.get() == '\n')
            {
              cout << "Good bye.\n";
              break;
            }
        }
        timer.sleep();
    } // while (true)
    ros::shutdown();

    return 0;
} // main

