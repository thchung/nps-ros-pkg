/**
 * File provides a shell for implementing publisher and subscriber
 * functionality in a single node.  The 
 * in
 */
#include <cmath>
#include <vector>
using namespace std;

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "sensor_msgs/LaserScan.h"


// NOTE: Path might need to be changed if package not named cs4313_launch
#include "robotUtilities.h"
#include "searchUtilities.h"



/** Global variable that will provide callback access to robot state */
RobotState state;
void getNextWaypoint();
bool targetFound = false;
bool victory = false;
double goal_x;
double goal_y;
int k = 0;
double waypoints[10][2] = {
{-11.1,-7},
{-9.7,5.4},
{-4.6,7},
{-6.1,-7},
{-1,-6.8},
{0.4,6.4},
{5.3,5.2},
{4.2,-4.8},
{10,-5.2},
{10,4}
};

/**
 * Callback for receipt of new odometry from the create.  This method will
 * parse state information from the nav_msgs/Odometry message and assign
 * it to the global state variable.
 */

void trueLocalization(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
    state.pose[X] = msg->pose.pose.position.x;
    state.pose[Y] = msg->pose.pose.position.y;
    state.pose[THETA] = yawFromQuaternion(msg->pose.pose.orientation);
 
    //ROS_INFO("Position: (%6.2f, %6.2f, %5.2f), Velocity: (%6.2f, %6.2f, %5.2f)",
    //    state.pose[X], state.pose[Y], state.pose[THETA],
    //   state.velocity[X], state.velocity[Y], state.velocity[THETA]);
} // trueLocaliaztion

void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
    //state.pose[X] = msg->pose.pose.position.x;
    //state.pose[Y] = msg->pose.pose.position.y;
    //state.pose[THETA] = yawFromQuaternion(msg->pose.pose.orientation);
    state.velocity[X] = msg->twist.twist.linear.x;
    state.velocity[Y] = msg->twist.twist.linear.y;
    state.velocity[THETA] = msg->twist.twist.angular.z;

    //ROS_INFO("Position: (%6.2f, %6.2f, %5.2f), Velocity: (%6.2f, %6.2f, %5.2f)",
    //    state.pose[X], state.pose[Y], state.pose[THETA],
    //    state.velocity[X], state.velocity[Y], state.velocity[THETA]);
} // odomCallback


/**
 * Callback for receipt of new laser data from the Hokuyo (or other) laser.
 * This method will parse state information from the sensor_msgs/LaserScan
 * message and assign it to the global robot state variable.
 */
void laserCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
    state.laserMinAngle = msg->angle_min;
    state.laserMaxAngle = msg->angle_max;
    state.laserAngleRes = msg->angle_increment;
    state.laserNumReturns = msg->ranges.size();
    state.laserMinRange = msg->range_min;
    state.laserMaxRange = msg->range_max;

    // Get the actual ranges from the message, but set anything greater than or
    // equal to maxRange or less than minRange to 0
    for (int rtn = 0; rtn < state.laserNumReturns; rtn++)
    {
        state.laserRanges[rtn] = msg->ranges[rtn];
        if ((state.laserRanges[rtn] >= state.laserMaxRange) ||
            (state.laserRanges[rtn] < state.laserMinRange))
            state.laserRanges[rtn] = 0.0;
    } // for (int rtn = 0;...
} // laserCallback

/**
 * Callback for receipt of target location and characteristics
 *  - (Point.x, Point.y) = center coordinates of the detected circle
 *  - (Point.z) = radius (in meters) of the detected circle
 */
void circleCallback(const geometry_msgs::Point::ConstPtr& msg)
{
  // Define number of consecutive times we must see the circle of desired size
  static int targetHits = 0;
  int  enoughTargetHits = 10;

  // Define local variable for circle parameter
  double radius = msg->z;

  // Check parameters for valid target
  double target_diam = 0.50;                // Known target diameter
  double threshold = 0.1 * target_diam;     // The diameter of the circle must be within 10% of the known target diameter

  if ( abs(2*radius-target_diam )<= threshold )  // If this target is within threshold
  {
    targetHits++;
  }
  else
  {
    targetHits = 0;
  }

  if (targetHits >= enoughTargetHits)
  {
    targetFound = true;
    printf("*****************************************\n");
    printf("*****************************************\n");
    printf("Target found at (%2.4f,%2.4f) with radius %2.4fm\n",
           msg->x, msg->y, radius);

    // print location in global coordinates
    printf("Target location is (%2.4f,%2.4f)\n",
           state.pose[X] + msg->x * cos(state.pose[THETA]) - msg->y * sin(state.pose[THETA]),
           state.pose[Y] + msg->x * sin(state.pose[THETA]) + msg->y * cos(state.pose[THETA]) );
  }
  else
  {
    targetFound = false;
  }
} // circleCallback

/**
 * Function should use the robot state information and decide how to command
 * the vehicle.  For now the function does not make any computations and 
 * simply publishes a fixed velocity to the "cmd_vel" topic.  These values
 * can be computed instead based on what the current state of the vehicle is
 * and we want it to do
 */
void sendCommand(ros::Publisher& pub)
{
    // Container for the command that will be published
    geometry_msgs::Twist command;

    double err_angle;		//Difference between current heading and intended heading
    double err_distance;	//Distance from current position to intended waypoint
    double angle_to_goal;		//
 
    double turn_gain = .5;	//Gain for proportional turn rate   
    double speed_gain = 0.05;	//Gain for propotional speed rate
    double min_speed = 0.0;	//Minimum speed for search motion
    double max_speed = 0.5;	//Maximum speed for search motion
		
    double laserdata[state.laserNumReturns][1];	//Processed laser bearing and range data matrix
    int i = 0;		//index for processing laser information into laserdata
    
    double repgain = 1;		//Gain factor for rep force
    double repmag;		//Magnitude for rep force
    double repdir;		//Polar direction for direction of rep force
    double force_rep_x;		//Computed x force component of rep force
    double force_rep_y;		//Computed y force component of rep force
    double sumforce_rep_x;	//Summation of x force component of rep force
    double sumforce_rep_y;	//Summation of y force component of rep force
   
    double attgain = 1;		//Gain factor for att force
    double attmag;		//Magnitude for att force
    double attdir;		//Polar direction for direction of att force
    double force_att_x;		//Computed x force component of att force
    double force_att_y;		//Computed y force component of att force
    double sumforce_att_x;	//Summation of x force component of att force
    double sumforce_att_y;	//Summation of y force component of att force

    double totalforce_x;	//Addition of att and rep force x component
    double totalforce_y;	//Addition of att and rep force y component

    

    err_distance = distance(state.pose[X], state.pose[Y], goal_x, goal_y);	//Error distance between goal and starting position
    angle_to_goal = headingTo(state.pose[X], state.pose[Y], goal_x, goal_y);	//

    	
    attmag = err_distance;			//Compute att mag and dir
    attdir = angle_to_goal-state.pose[THETA];

	for(i=0; i < state.laserNumReturns; i++)	//Index loop to compile processed laser data and compute x any y components of rep and att forces
	{
		laserdata[i][0] = state.laserMinAngle + i * state.laserAngleRes;//Index laser data into bearing and range matrix	
		laserdata[i][1] = state.laserRanges[i];
 		
		if(laserdata[i][1] != 0)
		{
			repmag = 1 / (laserdata[i][1] * laserdata[i][1]);	//Compute rep mag and dir
			repdir = normalizePI(laserdata[i][0] + M_PI);
			
			force_rep_x = repmag * cos(repdir);	//Compute rep force x and y components
			force_rep_y = repmag * sin(repdir);

			force_att_x = attmag * cos(attdir);	//Compute att force x and y components
			force_att_y = attmag * sin(attdir);

			sumforce_rep_x = sumforce_rep_x + force_rep_x;		//Summation of rep force x and y components
			sumforce_rep_y = sumforce_rep_y + force_rep_y;

			sumforce_att_x = sumforce_att_x + force_att_x;		//Summation of att force x and y components
			sumforce_att_y = sumforce_att_y + force_att_y;
		} //For loop if laser range is not zero
	} //Close for loop for index of laser and force data

	totalforce_x = attgain * sumforce_att_x + repgain * sumforce_rep_x;	//Sumation of att and rep forces x components			
	totalforce_y = attgain * sumforce_att_y + repgain * sumforce_rep_y;	//Sumation of att and rep forces y components
	
	printf("totalforce_x %2.4f\n", totalforce_x);		// print out info
	printf("totalforce_y %2.4f\n", totalforce_y);		// print out info

	err_angle = headingTo(0,0,totalforce_x,totalforce_y);	//Error angle between force heading and current heading 
	/*if (normalize2PI(headingTo(0,0,totalforce_x,totalforce_y)) > state.pose[THETA])
	{
		command.angular.z = 0.5;
	}
	if (normalize2PI(headingTo(0,0,totalforce_x,totalforce_y)) < state.pose[THETA])
	{	
		command.angular.z = -0.5;
	}*/
	command.angular.z = saturate(err_angle * turn_gain,-1,1);		//Proportional gain turn command
	if(fabs(err_angle) < 0.5)
	{
		command.linear.x = saturate(speed_gain * distance(0,0,totalforce_x,totalforce_y),min_speed,max_speed);  //Proportional speed command	
	}
	else
	{
		command.linear.x = 0.0;	
	}

	printf("true angle to goal is %2.4f\n", angle_to_goal);	
	printf("err_distance is %2.4f\n", err_distance);	
	printf("force heading is %2.4f\n", normalize2PI(headingTo(0,0,totalforce_x,totalforce_y)));
	printf("robot angle is %2.4f\n", state.pose[THETA]);
	//printf("unsaturated turn command is %2.4f\n", err_angle * turn_gain);		// print out info
	printf("turn command is %2.4f\n", command.angular.z);	
	printf("unsaturated speed command is %2.4f\n", speed_gain * distance(0,0,totalforce_x,totalforce_y));		// print out info

	//Check distance error and get next waypoint
    	if (err_distance < 0.2)    // if (distance error is within 0.1 meter threshold)
	{
		command.linear.x = 0.0;
		command.angular.z = 0.0;		
		pub.publish(command);

		getNextWaypoint();	// Set victory flag (i.e., victory = true;)
	}  //end if(running)

//  OUR APPROACH:  First turn, then run

//    	if (stillTurning) 
//  	{
//		command.linear.x = 0;        
//		// Check when to stop turning
//      	// Check your error angle within some threshold (e.g., 5.0 deg)
//        	if (angle_to_go < 2*M_PI/180)	//(angle within 2.0 deg)
//        	{
//			stillTurning = false; //toggle flag (i.e., stillTurning = false;)
//		}  //end if(within threshold)
//		command.angular.z = saturate(angle_to_go*4,2);
//  	}  //end if(still turning)
//    	else   // I should be running
//    	{
//		command.angular.z = saturate(angle_to_go*2,2);  //angle correction during run mode	
//		command.linear.x = saturate(err_distance*2,1);  //run mode speed control	

//		
//    	}  // end else(still turning)

    if (targetFound)
    {
      // For now commanding fwd speed of 0.2 m/s and rt turn of 0.2 rad/sec
      command.linear.x = 0.0;
      command.angular.z = 0.0;
    }
    //else
    //{
      // For now commanding fwd speed of 0.2 m/s and rt turn of 0.2 rad/sec
    //  command.linear.x = 1.0;
    //  command.angular.z = -1.0;
    //}

    pub.publish(command);
} // sendCommand

void getNextWaypoint() 
{
	goal_x = waypoints[k][0];
	goal_y = waypoints[k][1];

	k = k+1;

	if(k == 10)
	{
		victory = true;
	}

	/*printf("Enter x coordinate for next waypoint:\n");
	cin>>goal_x;
	printf("Enter y coordinate for next waypoint:\n");
	cin>>goal_y;*/
	
}

/**** Main ****/
int main(int argc, char **argv)
{
    ros::init(argc, argv, "simplePubSub_node");
    ros::NodeHandle node;

    // Subscription objects that will listen to the "odom" and "scan" topics
    ros::Subscriber odomSub = node.subscribe("odom", 1, odomCallback);
    ros::Subscriber laserSub = node.subscribe("base_scan/scan", 1, laserCallback);
    ros::Subscriber circleSub = node.subscribe("circle_detect", 1, circleCallback);
    ros::Subscriber psuedolocalization = node.subscribe("amcl_pose", 1, trueLocalization);

    // Publisher object that will publish commands to the "cmd_vel" topic
    ros::Publisher velPub = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);

    ros::Rate timer(10); // set control loop to run at 10 hz

    getNextWaypoint();

    while (ros::ok())   // can modify to have some "mission complete" criteria later
    {
        ros::spinOnce(); // Process all pending callbacks
        sendCommand(velPub);  // Decide what to do and publish commands
	
	if (targetFound)
        {
            printf("*****************************************\n");
            printf("*****************************************\n");
            printf("Target found!!!\n");
            cout << "Press the ENTER key to exit";
            if (cin.get() == '\n')
            {
              cout << "Good bye.\n";
              break;
            }
        }
        timer.sleep();
    } // while (true)
    ros::shutdown();

    return 0;
} // main

