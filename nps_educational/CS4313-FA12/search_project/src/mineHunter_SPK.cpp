/**
 * File provides a shell for implementing publisher and subscriber
 * functionality in a single node.  The 
 * in
 */
#include <deque>
#include <cmath>
#include <vector>
#include <iostream>
using namespace std;

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"


// NOTE: Path might need to be changed if package not named cs4313_launch
#include "robotUtilities.h"
#include "searchUtilities.h"



/** Global variable that will provide callback access to robot state */
RobotState state;
bool targetFound = false;

/** Global variables for a list of waypoints */
std::deque<Point> wayPointList;


/**
 * Callback for receipt of new odometry from the create.  This method will
 * parse state information from the nav_msgs/Odometry message and assign
 * it to the global state variable.
 */
void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
    state.pose[X] = msg->pose.pose.position.x;
    state.pose[Y] = msg->pose.pose.position.y;
    state.pose[THETA] = yawFromQuaternion(msg->pose.pose.orientation);
    state.velocity[X] = msg->twist.twist.linear.x;
    state.velocity[Y] = msg->twist.twist.linear.y;
    state.velocity[THETA] = msg->twist.twist.angular.z;

    ROS_INFO("Position: (%6.2f, %6.2f, %5.2f), Velocity: (%6.2f, %6.2f, %5.2f)",
        state.pose[X], state.pose[Y], state.pose[THETA],
        state.velocity[X], state.velocity[Y], state.velocity[THETA]);
} // odomCallback

/**
 * Callback for receipt of ground truth data from the simulator.  This method will
 * parse state information from the /amcl_pose message and assign
 * it to the global state variable.
 */
void amclCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
    state.pose[X] = msg->pose.pose.position.x;
    state.pose[Y] = msg->pose.pose.position.y;
    state.pose[THETA] = yawFromQuaternion(msg->pose.pose.orientation);

    ROS_INFO("Position: (%6.2f, %6.2f, %5.2f)  Goal WP: (%6.2f, %6.2f)",
        state.pose[X], state.pose[Y], state.pose[THETA],
        wayPointList.front().x,wayPointList.front().y);
} // amclCallback


/**
 * Callback for receipt of new laser data from the Hokuyo (or other) laser.
 * This method will parse state information from the sensor_msgs/LaserScan
 * message and assign it to the global robot state variable.
 */
void laserCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
    state.laserMinAngle = msg->angle_min;
    state.laserMaxAngle = msg->angle_max;
    state.laserAngleRes = msg->angle_increment;
    state.laserNumReturns = msg->ranges.size();
    state.laserMinRange = msg->range_min;
    state.laserMaxRange = msg->range_max;
    
    // Get the actual ranges from the message, but set anything greater than or
    // equal to maxRange or less than minRange to 0
    for (int rtn = 0; rtn < state.laserNumReturns; rtn++)
    {
        state.laserRanges[rtn] = msg->ranges[rtn];
        if ((state.laserRanges[rtn] >= state.laserMaxRange) ||
            (state.laserRanges[rtn] < state.laserMinRange))
            state.laserRanges[rtn] = 0.0;
    } // for (int rtn = 0;...
} // laserCallback

/**
 * Callback for receipt of target location and characteristics
 *  - (Point.x, Point.y) = center coordinates of the detected circle
 *  - (Point.z) = radius (in meters) of the detected circle
 */
void circleCallback(const geometry_msgs::Point::ConstPtr& msg)
{
    // Define number of consecutive times we must see the circle of desired size
    static int targetHits = 0;
    int  enoughTargetHits = 10;
    
    // Define local variable for circle parameter
    double radius = msg->z;

    // Check parameters for valid target
    double target_diam = 0.50;                // Known target diameter
    double threshold = 0.1 * target_diam;     // The diameter of the circle must be within 10% of the known target diameter

    if ( abs(2*radius-target_diam )<= threshold )  // If this target is within threshold
    {
      targetHits++;
    }
    else
    {
      targetHits = 0;
    }
    
    if (targetHits >= enoughTargetHits)
    {
      targetFound = true;
      printf("*****************************************\n");
      printf("*****************************************\n");
      printf("Target found at (%2.4f,%2.4f) with radius %2.4fm\n",
                msg->x, msg->y, radius);
                
      // print location in global coordinates
      printf("Target location is (%2.4f,%2.4f)\n",
             state.pose[X] + msg->x * cos(state.pose[THETA]) - msg->y * sin(state.pose[THETA]),
             state.pose[Y] + msg->x * sin(state.pose[THETA]) + msg->y * cos(state.pose[THETA]) );
    }
    else
    {
      targetFound = false;
    }

} // circleCallback


/**
 * Compute attractive force from artificial potential field as a function
 * of the robot's current position and the current waypoint.
 */
void apfAttract(Point& attForce)
{
  double zeta  = 5; // attractive force gain
  double dStar = 10.0; // saturation distance from current waypoint
  
  // compute distance from current waypoint
  double qLength = distance(state.pose[X],state.pose[Y],
                            wayPointList.front().x,wayPointList.front().y);
  
  // compute force components in world coordinate frame
  double FxW = -zeta * (state.pose[X] - wayPointList.front().x);
  double FyW = -zeta * (state.pose[Y] - wayPointList.front().y);
  if (qLength > dStar)
  {
    // saturate the attractive force if far from the goal
    FxW *= dStar / qLength;
    FyW *= dStar / qLength;
  }
  
  /*
  using namespace std;
  cerr << "qLen:" << qLength << " FxW:" << FxW << " FyW:" << FyW << endl;
  */
  
  // convert to robot coordinate frame
  attForce.x =  FxW * cos(state.pose[THETA]) + FyW * sin(state.pose[THETA]);
  attForce.y = -FxW * sin(state.pose[THETA]) + FyW * cos(state.pose[THETA]);
}


/**
 * Compute repulsive force from artificial potential field as a function
 * of the robot's current position and detected obstacles.
 */
void apfRepel(Point& repForce)
{
  double eta   = 0.25; // repulsive force gain
  double qStar = 2.0; // cut-off distance for obstacle influence
  
  // compute repulsive force components in robot coordinates
  double Fx = 0.0;
  double Fy = 0.0;

  // Sum the repulsive force components due to each laser return
  double range = 0.0;
  double rho = 0.0;
  double theta = state.laserMinAngle;
  double delta = state.laserMaxAngle - state.laserMinAngle;
  double numReturns = state.laserNumReturns;
  double angleRes;
  if (state.laserNumReturns > 0)
    angleRes = delta / (numReturns - 1.0);
  else
    angleRes = state.laserNumReturns;
  
  //cout << "#rtns:" << state.laserNumReturns << " delta:" << delta << " angleRes:" << angleRes << endl;
  
  // shave off a slice from both sides
  // TODO: normalize over number of valid returns (i.e. not zero)?
  int angleLimit = (int)( (60.0 * M_PI / 180.0) / angleRes);
  for (int rtn = angleLimit; rtn < (state.laserNumReturns - angleLimit); rtn++)
  {
    //theta += angleRes;
    theta = (double)state.laserMinAngle + rtn*angleRes;
    range = (double)state.laserRanges[rtn];
    
    using namespace std;
    
    /*
    cerr << rtn << " minAngle: " << state.laserMinAngle << 
                   " maxAngle: " << state.laserMaxAngle <<
                   " theta: " << theta << endl;
    */
    
    // check for error
    if (theta > (double)state.laserMaxAngle)
    {
      theta = (double)state.laserMaxAngle;
      /*
      cerr << "***** Computed theta[" << rtn << "/"
            << state.laserNumReturns << "] = " << theta << " *****" << endl;
      exit(0);
      */
    }
    
    if ( (range > 0.0) && (range <= qStar) )
    {
      rho = ( (1.0/qStar) - (1.0/range) ) / pow(range,2.0);
      Fx += rho * cos(theta);
      Fy += rho * sin(theta);
    }
  }
  repForce.x = eta * Fx;
  repForce.y = eta * Fy;
}


/**
 * Function to compute robot commands from total artificial potential field
 */
void apfControl(geometry_msgs::Twist& command)
{
  // controller gains
    double linGain = 0.1;
    double angGain = 0.35;
    double angGainP = 0.5;
    double angGainD = 0.5;
    
    // maximum velocity commands
    double minLinearVel = 0.5;//0.2;
    double maxLinearVel = 1.2;
    double minAngularVel = -1.0;
    double maxAngularVel =  1.0;
    
    // signal robot to stop when apfTotal.x is too negative
    double angularGainInc = 1.0;
    double panicThreshold = -200.0;
    
    // compute potential field components
    Point apfAtt, apfRep, apfTotal;
    
    apfAttract(apfAtt);
    apfRepel(apfRep);
    
    /*
    ROS_INFO("attX:%6.2f, attY: %6.2f, repX:%6.2f, repY: %6.2f",
              apfAtt.x, apfAtt.y, apfRep.x, apfRep.y);
    */
    
    apfTotal.x = apfAtt.x + apfRep.x;
    apfTotal.y = apfAtt.y + apfRep.y;
    
    double range = distance(state.pose[X],state.pose[Y],
                            wayPointList.front().x,wayPointList.front().y);
                            
    double targetHdgErr = headingTo(state.pose[X],state.pose[Y],
                            wayPointList.front().x,wayPointList.front().y) - state.pose[THETA];
    targetHdgErr = normalizePI(targetHdgErr);                            
                            
    // logic for reaching waypoint
    double watchRadius = 0.35;
    if (range < watchRadius)
    {
      ROS_INFO("********* Waypoint (%3.1lf,%3.1lf) Reached! *********",
               wayPointList.front().x,wayPointList.front().y);
      
      wayPointList.pop_front();
    }
    else
    {
      command.linear.x  = linGain * apfTotal.x;
      
      // use sin function to bound angular velocity to range (-1.0,1.0)
      command.angular.z = angGain * sin( atan2(apfTotal.y,apfTotal.x) );
    }
    
    // check if we have reached all waypoints (and not found the target)
    if (wayPointList.empty())
    {
      command.linear.x = 0.0;
      command.angular.z = 0.0;
      ROS_INFO("********* Final Waypoint Reached! *********");
      return;
    }

    // increase turn command if our ATTRACTIVE potential is negative (we're facing wrong way)
    if (apfAtt.x < 0.0)
      command.angular.z *= angularGainInc;
    
    // NOTE: THIS ISN'T VERY HELPFUL
    // turn in place if we are facing the wrong way)
    if ( fabs(targetHdgErr) > (90.0 * M_PI / 180.0) )
    {
      command.linear.x = saturate(command.linear.x, 0.10, 0.15);
    }
    else if ( fabs(targetHdgErr) > (30.0 * M_PI / 180.0) )
    {
      command.linear.x = saturate(command.linear.x, 0.2, 0.3);;
    }
    else if ( fabs(targetHdgErr) > (15.0 * M_PI / 180.0) )
    {
      command.linear.x = saturate(command.linear.x, 0.3, 0.5);;
    }
    else
    {
      // bound the linear velocity command
      command.linear.x = saturate(command.linear.x, minLinearVel, maxLinearVel);
    }
    
    // bound the angular velocity command
    command.angular.z = saturate(command.angular.z, minAngularVel, maxAngularVel);
    
    // panic if we are too close to a wall
    if ( apfTotal.x < panicThreshold )
    {
      command.linear.x = 0.0;
      command.angular.z = command.angular.z / fabs(command.angular.z);
    }
    
    // TODO: turn in place if heading error is greater than 15 degrees?
    // TODO: use PID control for turn rate command
    
    //*
    ROS_INFO("apfX:%6.2f, apfY: %6.2f, hdgToWP: %6.2f, LinCmd:%6.2f, AngCmd: %6.2f",
      apfTotal.x, apfTotal.y, targetHdgErr, command.linear.x, command.angular.z);
    //*/
    
    /*
    ROS_INFO("hdgCmd:%6.2f, angErr:%6.2f, LinCmd: %6.2f, AngCmd: %6.2f",
      hdgCmd, angErr, command.linear.x, command.angular.z);
    */    
}


/**
 * This function generates a new waypoint from a laydown pattern based on known area boundaries
 * *** CURRENTLY NOT IMPLEMENTED OR USED ***
 */
void genNextWaypoint()
{
  double xBounds[4] = {-5.0,-5.0, 5.0, 5.0};
  double yBounds[4] = {-5.0, 5.0, 5.0,-5.0};
  
  Point waypoint;
  
  // path spacing
  double scanHalfWidth = 0.5;
  double rho, theta;
  
  /*
  // Compute rho, theta for the first boundary, then lay-down paths parallel to it
  theta = atan((X(2) - X(1))/(Y(1) - Y(2)))
  rho = X(1)*cos(theta) + Y(1)*sin(theta)
  */
}


/**
 * Function should use the robot state information and decide how to command
 * the vehicle.
 */
void sendCommand(ros::Publisher& pub)
{
    // Container for the command that will be published
    geometry_msgs::Twist command;

    if (targetFound)
    {
      // For now commanding fwd speed of 0.2 m/s and rt turn of 0.2 rad/sec
      command.linear.x = 0.0;
      command.angular.z = 0.0;
    }
    else
    {
      // For now commanding fwd speed of 0.2 m/s and rt turn of 0.2 rad/sec
      // command.linear.x = 1.0;
      // command.angular.z = -1.0;
      
      apfControl(command);
    }

    pub.publish(command);
} // sendCommand


/**** Main ****/
int main(int argc, char **argv)
{
  /**
   * Create a list of waypoints
   */
  Point waypoint;
  
  waypoint.x = -11.1;
  waypoint.y = -7.0;
  wayPointList.push_back(waypoint);
  
  waypoint.x = -9.7;
  waypoint.y = 5.4;
  wayPointList.push_back(waypoint);
  
  waypoint.x = -4.6;
  waypoint.y = 7.0;
  wayPointList.push_back(waypoint);
  
  waypoint.x = -6.1;
  waypoint.y = -7.0;
  wayPointList.push_back(waypoint);
  
  waypoint.x = -1.0;
  waypoint.y = -6.8;
  wayPointList.push_back(waypoint);
  
  waypoint.x = 0.4;
  waypoint.y = 6.4;
  wayPointList.push_back(waypoint);
  
  waypoint.x = 5.3;
  waypoint.y = 5.2;
  wayPointList.push_back(waypoint);
  
  waypoint.x = 4.2;
  waypoint.y = -5.2;
  wayPointList.push_back(waypoint);
  
  waypoint.x = 10.0;
  waypoint.y = -5.2;
  wayPointList.push_back(waypoint);
  
  waypoint.x = 10.0;
  waypoint.y = 4.0;
  wayPointList.push_back(waypoint);
  
  /* DEBUG
  using namespace std;
  cout << "waypoints:" << endl;
  for (unsigned int i=0; i<wayPointList.size(); i++)
  {
    cout << "(" << wayPointList[i].pose[X] << ","
         << wayPointList[i].pose[Y] << ")" << endl;
  }
  exit(0);
  */
  
    ros::init(argc, argv, "simplePubSub_node");
    ros::NodeHandle node;

    // Subscription objects that will listen to the "odom" and "scan" topics
    //ros::Subscriber odomSub = node.subscribe("odom", 1, odomCallback);
    ros::Subscriber odomSub = node.subscribe("amcl_pose", 1, amclCallback);
    ros::Subscriber laserSub = node.subscribe("scan", 1, laserCallback);
    ros::Subscriber circleSub = node.subscribe("circle_detect", 1, circleCallback);

    // Publisher object that will publish commands to the "cmd_vel" topic
    ros::Publisher velPub = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);

    ros::Rate timer(10); // set control loop to run at 10 hz

    //while (ros::ok())   // can modify to have some "mission complete" criteria later
    while (!wayPointList.empty())   // can modify to have some "mission complete" criteria later
    {
        ros::spinOnce(); // Process all pending callbacks
        sendCommand(velPub);  // Decide what to do and publish commands

        if (targetFound)
        {
            printf("*****************************************\n");
            printf("*****************************************\n");
            printf("Target found!!!\n");
            cout << "Press the ENTER key to exit";
            if (cin.get() == '\n')
            {
              cout << "Good bye.\n";
              break;
            }
        }
        timer.sleep();
    } // while (true)
    ros::shutdown();

    return 0;
} // main

