/**
 * File provides a shell for implementing publisher and subscriber
 * functionality in a single node.  The 
 * in
 */
#include <cmath>
#include <vector>
using namespace std;

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"

// NOTE: Path might need to be changed if package not named cs4313_launch
#include "robotUtilities.h"
#include "searchUtilities.h"



/** Global variable that will provide callback access to robot state */
RobotState state;
bool targetFound = false;

/**
 * Callback for receipt of new odometry from the create.  This method will
 * parse state information from the nav_msgs/Odometry message and assign
 * it to the global state variable.
 */
void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
    //    state.pose[X] = msg->pose.pose.position.x;
    //    state.pose[Y] = msg->pose.pose.position.y;
    //    state.pose[THETA] = yawFromQuaternion(msg->pose.pose.orientation);
    state.velocity[X] = msg->twist.twist.linear.x;
    state.velocity[Y] = msg->twist.twist.linear.y;
    state.velocity[THETA] = msg->twist.twist.angular.z;

    //    ROS_INFO("Position: (%6.2f, %6.2f, %5.2f), Velocity: (%6.2f, %6.2f, %5.2f)",
    //        state.pose[X], state.pose[Y], state.pose[THETA],
    //        state.velocity[X], state.velocity[Y], state.velocity[THETA]);
} // odomCallback


/**
 * Callback for receipt of new laser data from the Hokuyo (or other) laser.
 * This method will parse state information from the sensor_msgs/LaserScan
 * message and assign it to the global robot state variable.
 */
void laserCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
    state.laserMinAngle = msg->angle_min;
    state.laserMaxAngle = msg->angle_max;
    state.laserAngleRes = msg->angle_increment;
    state.laserNumReturns = msg->ranges.size();
    state.laserMinRange = msg->range_min;
    state.laserMaxRange = msg->range_max;

    // Get the actual ranges from the message, but set anything greater than or
    // equal to maxRange or less than minRange to 0
    for (int rtn = 0; rtn < state.laserNumReturns; rtn++)
    {
        state.laserRanges[rtn] = msg->ranges[rtn];
        if ((state.laserRanges[rtn] >= state.laserMaxRange) ||
            (state.laserRanges[rtn] < state.laserMinRange))
            state.laserRanges[rtn] = 0.0;
    } // for (int rtn = 0;...
} // laserCallback

/**
 * Callback for receipt of localization filter pose estimate
 */
void amclPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
    state.pose[X] = msg->pose.pose.position.x;
    state.pose[Y] = msg->pose.pose.position.y;
    state.pose[THETA] = yawFromQuaternion(msg->pose.pose.orientation);

    ROS_INFO("Position: (%6.2f, %6.2f, %5.2f)", state.pose[X], state.pose[Y], state.pose[THETA]);
} // amclPoseCallback

/**
 * Callback for receipt of target location and characteristics
 *  - (Point.x, Point.y) = center coordinates of the detected circle
 *  - (Point.z) = radius (in meters) of the detected circle
 */
void circleCallback(const geometry_msgs::Point::ConstPtr& msg)
{
    // Define local variable for circle parameter
    double radius = msg->z;

    // Check parameters for valid target
    double target_diam = 0.50;                // Known target diameter
    double threshold = 0.1 * target_diam;     // The diameter of the circle must be within 10% of the known target diameter

    if ( abs(2*radius-target_diam )<= threshold )  // If this target is within threshold
    {
        targetFound = true;
        printf("*****************************************\n");
        printf("*****************************************\n");
        printf("Target found at (%2.4f,%2.4f) with radius %2.4fm\n", msg->x, msg->y, radius);
   }


} // circleCallback


/**
 * Function should use the robot state information and decide how to command
 * the vehicle.  For now the function does not make any computations and 
 * simply publishes a fixed velocity to the "cmd_vel" topic.  These values
 * can be computed instead based on what the current state of the vehicle is
 * and we want it to do
 */
void sendCommand(ros::Publisher& pub)
{
    // Container for the command that will be published
    geometry_msgs::Twist command;

    if (targetFound)
    {
      // For now commanding fwd speed of 0.2 m/s and rt turn of 0.2 rad/sec
      command.linear.x = 0.0;
      command.angular.z = 0.0;
    }
    else
    {
      // For now commanding fwd speed of 0.2 m/s and rt turn of 0.2 rad/sec
      command.linear.x = 1.0;
      command.angular.z = -1.0;
    }

    pub.publish(command);
} // sendCommand


/**** Main ****/
int main(int argc, char **argv)
{
    ros::init(argc, argv, "mineHunter_node");
    ros::NodeHandle node;

    // Subscription objects that will listen to the "odom" and "scan" topics
    ros::Subscriber odomSub = node.subscribe("odom", 1, odomCallback);
    ros::Subscriber amclPoseSub = node.subscribe("amcl_pose", 1, amclPoseCallback);
    ros::Subscriber laserSub = node.subscribe("scan", 1, laserCallback);
    ros::Subscriber circleSub = node.subscribe("circle_detect", 1, circleCallback);

    // Publisher object that will publish commands to the "cmd_vel" topic
    ros::Publisher velPub = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);

    ros::Rate timer(10); // set control loop to run at 10 hz

    while (ros::ok())   // can modify to have some "mission complete" criteria later
    {
        ros::spinOnce(); // Process all pending callbacks
        sendCommand(velPub);  // Decide what to do and publish commands

        if (targetFound)
        {
            printf("*****************************************\n");
            printf("*****************************************\n");
            printf("Target found!!!\n");
            cout << "Press the ENTER key to exit";
            if (cin.get() == '\n')
            {
              cout << "Good bye.\n";
              break;
            }
        }
        timer.sleep();
    } // while (true)
    ros::shutdown();

    return 0;
} // main

