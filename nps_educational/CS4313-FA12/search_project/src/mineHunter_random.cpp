/**
 * File provides a shell for implementing publisher and subscriber
 * functionality in a single node.  The 
 * in
 */
#include <cmath>
#include <vector>
using namespace std;

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"


// NOTE: Path might need to be changed if package not named cs4313_launch
#include "robotUtilities.h"
#include "searchUtilities.h"



/** Global variable that will provide callback access to robot state */
RobotState state;
bool targetFound = false;
// Container for the command that will be published
geometry_msgs::Twist command;

bool inRunningMode = true;          // suppose we're in running mode by default

/** SPS */
bool stillTurning = true;
bool victory = false;
double goal_x = 4;
double goal_y = 2;


/**
 * Callback for receipt of new odometry from the create.  This method will
 * parse state information from the nav_msgs/Odometry message and assign
 * it to the global state variable.
 */
void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  state.pose[X] = msg->pose.pose.position.x;
  state.pose[Y] = msg->pose.pose.position.y;
  state.pose[THETA] = yawFromQuaternion(msg->pose.pose.orientation);
  state.velocity[X] = msg->twist.twist.linear.x;
  state.velocity[Y] = msg->twist.twist.linear.y;
  state.velocity[THETA] = msg->twist.twist.angular.z;

  ROS_INFO("Position: (%6.2f, %6.2f, %5.2f), Velocity: (%6.2f, %6.2f, %5.2f)",
           state.pose[X], state.pose[Y], state.pose[THETA],
           state.velocity[X], state.velocity[Y], state.velocity[THETA]);
} // odomCallback


/**
 * Callback for receipt of new laser data from the Hokuyo (or other) laser.
 * This method will parse state information from the sensor_msgs/LaserScan
 * message and assign it to the global robot state variable.
 */
void laserCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
  state.laserMinAngle = msg->angle_min;
  state.laserMaxAngle = msg->angle_max;
  state.laserAngleRes = msg->angle_increment;
  state.laserNumReturns = msg->ranges.size();
  state.laserMinRange = msg->range_min;
  state.laserMaxRange = msg->range_max;

  // Get the actual ranges from the message, but set anything greater than or
  // equal to maxRange or less than minRange to 0
  for (int rtn = 0; rtn < state.laserNumReturns; rtn++)
  {
    state.laserRanges[rtn] = msg->ranges[rtn];
    if ((state.laserRanges[rtn] >= state.laserMaxRange) ||
        (state.laserRanges[rtn] < state.laserMinRange))
      state.laserRanges[rtn] = 0.0;
  } // for (int rtn = 0;...
} // laserCallback

/**
 * Callback for receipt of target location and characteristics
 *  - (Point.x, Point.y) = center coordinates of the detected circle
 *  - (Point.z) = radius (in meters) of the detected circle
 */
void circleCallback(const geometry_msgs::Point::ConstPtr& msg)
{
  // Define number of consecutive times we must see the circle of desired size
  static int targetHits = 0;
  int  enoughTargetHits = 10;

  // Define local variable for circle parameter
  double radius = msg->z;

  // Check parameters for valid target
  double target_diam = 0.50;                // Known target diameter
  double threshold = 0.1 * target_diam;     // The diameter of the circle must be within 10% of the known target diameter

  if ( abs(2*radius-target_diam )<= threshold )  // If this target is within threshold
  {
    targetHits++;
  }
  else
  {
    targetHits = 0;
  }

  if (targetHits >= enoughTargetHits)
  {
    targetFound = true;
    printf("*****************************************\n");
    printf("*****************************************\n");
    printf("Target found at (%2.4f,%2.4f) with radius %2.4fm\n",
           msg->x, msg->y, radius);

    // print location in global coordinates
    printf("Target location is (%2.4f,%2.4f)\n",
           state.pose[X] + msg->x * cos(state.pose[THETA]) - msg->y * sin(state.pose[THETA]),
           state.pose[Y] + msg->x * sin(state.pose[THETA]) + msg->y * cos(state.pose[THETA]) );
  }
  else
  {
    targetFound = false;
  }


} // circleCallback

void sendCommand(ros::Publisher& pub)
{

  if (targetFound)
  {
    command.linear.x = 0.0;
    command.angular.z = 0.0;
  }

  else

  {
    double err_angle;
    double err_distance;
    double angle_to_go;
    double laserData[state.laserNumReturns][2];
    double randomangle;
    err_angle = headingTo(state.pose[X], state.pose[Y], goal_x, goal_y);	//Normalized Error angle between goal and starting position
    angle_to_go = normalizePI(randomangle - state.pose[THETA]);
    err_distance = distance(state.pose[X], state.pose[Y], goal_x, goal_y);	//Error distance between goal and starting position

    for (int i = 0; i < state.laserNumReturns; i++) //i = scan increment
    {
      laserData[i][0] = state.laserMinAngle + i*state.laserAngleRes;
      laserData[i][1] = state.laserRanges[i];
      // printf( "Laser data: %2.4f, %2.4f\n", laserData[i][0], laserData[i][1] );
    }

    if ( inRunningMode )                // still in running mode
    {
      command.linear.x = 0.6;
      command.angular.z = 0.0;         
      // Check if there's an obstacle ahead in any one of the laser beams
      for (int j = round(state.laserNumReturns/2)-30; j < round(state.laserNumReturns/2)+30; j++)
      {
        //printf( "My laserdata is %2.4f, %2.4f\n", laserData[j][0], laserData[j][1] );

        // Check if laserData range returns are 0 OR greater than 1m more than minimum range
        //  If so, then you are in 'go straight forward' mode
        if(laserData[j][1] == 0 || laserData[j][1] >= (state.laserMinRange + 1.0))
        {
          inRunningMode = true;         // affirm that there is no obstacle ahead and still in running mode
        }
        else
        {
          // There IS an obstacle ahead so prepare parameters
          inRunningMode = false;        // no longer in running mode since obstacle ahead; must be in 'turning mode'

          // Only want to compute this random number ONCE at the beginning of turning mode, rather than at each time step
          randomangle = 2*M_PI * ( rand() / (RAND_MAX + 1.0) );        // This now normalizes the random number to be within [0,1]
          printf( "Random angle is: %2.4f\n", randomangle    );
          break;        // to get out of the for-loop iterating over all laserbeams
        }       // end else
      }         // end for(j)
    }           // end check(inRunningMode)

    else                // must be in turning mode
    {
      // Turning mode means nonzero turning speed and zero forward speed
      command.linear.x = 0.0;
      command.angular.z=0.5;
      angle_to_go = normalizePI(randomangle - state.pose[THETA]);  	
      // ********  command.angular.z = saturate(angle_to_go*10,4);
      printf( "Angular Velocity is: %2.4f\n", command.angular.z    );  

      // Check if we want to transition back to running mode
      //  We transition if the angle error is within some tolerance (since we know we won't hit the random angle exactly)

      if ( abs( state.pose[THETA]-randomangle ) < 0.20 )        // difference between current THETA and random angle is within 0.10 radians
      {
        // No longer in turning mode, but rather transition back to running mode
        inRunningMode = true;
      }

      else
      {
        inRunningMode = false;          // remain in transit mode; this 'else' section isn't necessary but included for extra clarity
      }
    }           // end else(inRunningMode)
  }  // end else(check for targetFound)


  pub.publish(command);
}; // sendCommand



/**** Main ****/
int main(int argc, char **argv)
{
  ros::init(argc, argv, "simplePubSub_node");
  ros::NodeHandle node;

  // Subscription objects that will listen to the "odom" and "scan" topicsT
  ros::Subscriber odomSub = node.subscribe("odom", 1, odomCallback);
  ros::Subscriber laserSub = node.subscribe("base_scan/scan", 1, laserCallback);
  ros::Subscriber circleSub = node.subscribe("circle_detect", 1, circleCallback);
  // Publisher object that will publish commands to the "cmd_vel" topic
  ros::Publisher velPub = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);

  ros::Rate timer(10); // set control loop to run at 10 hz

  while (ros::ok())   // can modify to have some "mission complete" criteria later
  {
    ros::spinOnce(); // Process all pending callbacks
    sendCommand(velPub);  // Decide what to do and publish commands

    if (targetFound)
    {
      printf("*****************************************\n");
      printf("*****************************************\n");
      printf("Target found!!!\n");
      cout << "Press the ENTER key to exit";

      if (cin.get() == '\n')
      {
        cout << "Good bye.\n";
        break;
      }
    }
    timer.sleep();
  } // while (true)
  ros::shutdown();

  return 0;
} // main

