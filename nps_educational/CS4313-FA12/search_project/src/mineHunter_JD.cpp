
#include <cmath>
#include <vector>
using namespace std;

#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"


#include <iostream>

// NOTE: Path might need to be changed if package not named cs4313_launch
#include "robotUtilities.h"
#include "searchUtilities.h"

struct RobotState2
{
    //double 	pose[4];
    //double 	velocity[3];
    double 	waypoint[2];
    //double 	interimWaypoint[2];
    //float 	laserMinAngle;
    //float 	laserMaxAngle;
    //float 	laserAngleRes;
    //int 	laserNumReturns;
    //float 	laserMinRange;
    //float 	laserMaxRange;
    //float 	laserRanges[1000];
}; 

/** Global variable that will provide callback access to robot state */
RobotState state;
RobotState2 state2;
bool targetFound = false;



/**
 * Callback for receipt of localization filter pose estimate
 */
void amclPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
    state.pose[X] = msg->pose.pose.position.x;
    state.pose[Y] = msg->pose.pose.position.y;
    state.pose[THETA] = yawFromQuaternion(msg->pose.pose.orientation);

    ROS_INFO("Position: (%6.2f, %6.2f, %5.2f)", state.pose[X], state.pose[Y], state.pose[THETA]);
} // amclPoseCallback

/**
 * Callback for receipt of target location and characteristics
 *  - (Point.x, Point.y) = center coordinates of the detected circle
 *  - (Point.z) = radius (in meters) of the detected circle
 */
void circleCallback(const geometry_msgs::Point::ConstPtr& msg)
{
    // Define local variable for circle parameter
    double radius = msg->z;

    // Check parameters for valid target
    double target_diam = 0.50;                // Known target diameter
    double threshold = 0.1 * target_diam;     // The diameter of the circle must be within 10% of the known target diameter

    if ( abs(2*radius-target_diam )<= threshold )  // If this target is within threshold
    {
        targetFound = true;
        printf("*****************************************\n");
        printf("*****************************************\n");
        printf("Target found at (%2.4f,%2.4f) with radius %2.4fm\n", msg->x, msg->y, radius);
   }


} // circleCallback


/**
 * Function should use the robot state information and decide how to command
 * the vehicle.  For now the function does not make any computations and 
 * simply publishes a fixed velocity to the "cmd_vel" topic.  These values
 * can be computed instead based on what the current state of the vehicle is
 * and we want it to do
 */





using namespace std;

double *RepulsiveForce(double obstacleInfluence);  //
double *AttractiveForce(double xCoor, double yCoor); //
double getTurn(double targetAngle); //
double getAngle(double x, double y);//
double getVelocity(double x, double y, double maxVel);//
double myOrientation();





void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
    //state.pose[X] 		= msg->pose.pose.position.x;
    //state.pose[Y] 		= msg->pose.pose.position.y;
    //state.pose[THETA] 		= yawFromQuaternion(msg->pose.pose.orientation);
    state.velocity[X] 		= msg->twist.twist.linear.x;
    state.velocity[Y] 		= msg->twist.twist.linear.y;
    state.velocity[THETA] 	= msg->twist.twist.angular.z;
}

void laserCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
    state.laserMinAngle 	= msg->angle_min;
    state.laserMaxAngle 	= msg->angle_max;
    state.laserAngleRes 	= msg->angle_increment;
    state.laserNumReturns 	= msg->ranges.size();
    state.laserMinRange 	= msg->range_min;
    state.laserMaxRange 	= msg->range_max;

    for (int rtn = 0; rtn < state.laserNumReturns; rtn++)
    {
        state.laserRanges[rtn] = msg->ranges[rtn];
        if ((state.laserRanges[rtn] >= state.laserMaxRange) ||
            (state.laserRanges[rtn] < state.laserMinRange))
            state.laserRanges[rtn] = 0.0;
    }
}

void sendCommand(ros::Publisher& pub, float vel, float ang)
{
    geometry_msgs::Twist command;

    command.linear.x 	= vel;
    command.angular.z 	= ang;

    pub.publish(command);
}

double myOrientation(){
	return (state.pose[THETA]);
}

double getVelocity(double x, double y, double maxVel){
	double velocity = sqrt(x*x + y*y);
	//if (abs(getAngle(x, y)) >= 1.0) return 0;
	//if (abs(getAngle(x, y)) >= 0.5) return state.velocity[X] - 0.01;
	//if (state.velocity[X] >= velocity * maxVel) return state.velocity[X] - 0.1;
	//if (state.velocity[X] < velocity*maxVel) return state.velocity[X] + 0.1;
	//if (velocity >= maxVel) return maxVel;
	return velocity;
}

double getAngle(double x, double y){
	return normalizePI(atan2(y, x));
}

double getTurn(double targetAngle){
	double myAng 		= 0.0;
	double direction 	= 1.0;
	double navError 	= (targetAngle);

	if (navError < 0) direction = -1.0;	
	if (abs(navError) < 1 ) myAng = navError;
	if (abs(navError) >= 1 ) {
		myAng = 1 * direction;
	}
	

	
	return myAng;
}

double *AttractiveForce(double xCoor, double yCoor){
	double d 	= sqrt(pow((state.pose[X] - xCoor),2) + pow((state.pose[Y]-yCoor),2));
	double theta 	= normalizePI(atan2(yCoor - state.pose[Y], xCoor-state.pose[X])-myOrientation()); //normalizePI(normalizePI(atan2(state.pose[Y]-yCoor, state.pose[X]-xCoor))-myOrientation());

	cout << "d: ";
	cout << d << endl;

	cout << "theta: ";
	cout << theta << endl;

	double x 	= d * cos(theta);
	double y 	= d * sin (theta);
	double attrGain = 1;
	double deltaX 	= -(attrGain * x/(sqrt(pow(x,2) + pow(y,2))));
	double deltaY 	= -(attrGain * y/(sqrt(pow(x,2) + pow(y,2))));
	double * Delta = new double[2];

	Delta[0] = deltaX; 
	Delta[1] = deltaY; 
	return Delta;
}

double *RepulsiveForce(double obstacleInfluence){
	double deltaX;
	double deltaY;
	double d; 
	double x;
	double y;
	double theta;
	double repGain 		= 0.0; //0.001 nearly balances attraction at 1.0 obstacle influence
	double sumDeltaX	= 0.0;
	double sumDeltaY 	= 0.0;

	for (int i = 0; i < state.laserNumReturns; i++){
		repGain = 0.0;
		d = state.laserRanges[i];
		theta = (state.laserAngleRes*i + state.laserMinAngle);
		if (d > 0.0){  //00005 and 005
			//if ((i<=150) and (i >= state.laserNumReturns - 150)) repGain = 0.1;			
			//if ((i <= 145) or (i > state.laserNumReturns - 145)) repGain = 0.01;
			if ((i >= 320) or (i <= state.laserNumReturns - 320)) repGain = 0.00001;
			if ((i >= 335) or (i <= state.laserNumReturns - 335)) repGain = 0.0005;
			x 	= d * cos(theta);
			y	= d * sin (theta);
			deltaX 	= repGain * 2 * x / pow((pow(x,2) + pow(y,2)),2);
			deltaY 	= repGain * 2 * y / pow((pow(x,2) + pow(y,2)),2);

		sumDeltaX 	= sumDeltaX + deltaX;
		sumDeltaY 	= sumDeltaY + deltaY;
		}
	}

	double * Delta 	= new double[2];
	Delta[0] 	= sumDeltaX; 
	Delta[1] 	= sumDeltaY; 
	return Delta;
}

void commandSelect(ros::Publisher& pub){
	int myState 		= 1;
	double *attraction	= 0;
	double *repulsion	= 0;
	double maxVel 		= 3.0;
	double deltaX;
	double deltaY;
	double myVel;
	double myAng;
	double myTurn;
	double obstInfl 	= 1.0;


	double distance = sqrt(pow((state.pose[X] - state2.waypoint[X]),2) + pow((state.pose[Y] - state2.waypoint[Y]),2));
	if (distance <= 0.25){
		myState = 0;  // at waypoint
	};

	switch (myState){
		case 1:
			attraction 	= AttractiveForce(state2.waypoint[X], state2.waypoint[Y]);			
			repulsion 	= RepulsiveForce(obstInfl);

			deltaX 		= -(attraction[0] + repulsion[0]);
			deltaY 		= -(attraction[1] + repulsion[1]);

			
			myAng 		= getAngle(deltaX, deltaY);
			myTurn 		= getTurn(myAng);

			if (abs(myTurn) >= 0.5)	myVel = 0.25;
			else myVel = 1;//getVelocity(deltaX, deltaY, maxVel);
			
			
			cout << state.laserMinAngle <<endl;
			cout << state.laserMaxAngle <<endl;
			cout << state.laserAngleRes << endl;
			cout << "Att X: ";	
			cout << attraction[0] << endl;
			cout << "Att Y: ";	
			cout << attraction[1] << endl;
			cout << "Rep X: ";	
			cout << repulsion[0] << endl;
			cout << "Rep Y: ";	
			cout << repulsion[1] << endl;
			cout << "Delta X: ";	
			cout << deltaX << endl;
			cout << "Delta Y: ";	
			cout << deltaY << endl;
			cout << "Potential Field Velocity: ";
			cout << myVel << endl;
			cout << "Potential Field Angle: ";
			cout << myAng << endl;
			cout << "My Bearing (Angle): ";
			cout << myOrientation() << endl;
			cout << "Turn Command: ";
			cout << myTurn << endl;	
			cout << "Current X: ";	
			cout << state.pose[X] << endl;
			cout << "Current Y: ";	
			cout << state.pose[Y] << endl;

			sendCommand(pub, myVel, myTurn);

			break;
		case 0:
			sendCommand(pub, 0, 0);

			cout << "Enter waypoint X coordinate: " << endl;
			cin >> state2.waypoint[X];
			cout << "Enter waypoint Y coordinate: " << endl;
			cin >> state2.waypoint[Y];
			break;
	}
}




/**** Main ****/
int main(int argc, char **argv)
{
	state2.waypoint[X] = 0;
 	state2.waypoint[Y] = 0;

    ros::init(argc, argv, "mineHunter_node");
    ros::NodeHandle node;



    // Subscription objects that will listen to the "odom" and "scan" topics
    ros::Subscriber odomSub = node.subscribe("odom", 1, odomCallback);
    ros::Subscriber amclPoseSub = node.subscribe("amcl_pose", 1, amclPoseCallback);
    //ros::Subscriber laserSub = node.subscribe("scan", 1, laserCallback);
    ros::Subscriber laserSub = node.subscribe("base_scan/scan", 1, laserCallback);
    ros::Subscriber circleSub = node.subscribe("circle_detect", 1, circleCallback);

    // Publisher object that will publish commands to the "cmd_vel" topic
    ros::Publisher velPub = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);

    ros::Rate timer(100); // set control loop to run at 10 hz

    while (ros::ok())   // can modify to have some "mission complete" criteria later
    {
		ros::spinOnce(); // Process all pending callbacks      	
    		commandSelect(velPub);

        if (targetFound)
        {
            printf("*****************************************\n");
            printf("*****************************************\n");
            printf("Target found!!!\n");
            cout << "Press the ENTER key to exit";
            if (cin.get() == '\n')
            {
              cout << "Good bye.\n";
              break;
            }
        }
        timer.sleep();
    } // while (true)
    ros::shutdown();

    return 0;
} // main

