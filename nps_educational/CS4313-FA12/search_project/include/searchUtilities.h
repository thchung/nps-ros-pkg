/**
 * File contains utility functions for working with ground robots using
 * the Robot Operating System (ROS)
 */
#ifndef SEARCH_UTILITIES_H
#define SEARCH_UTILITIES_H

#include <cmath>



/**
 * Point geometry structure
 */
struct Point
{
    /** Robot position specified as (x, y, theta) */
    double x;
    double y;
}; // Point

/**
 * Circle geometry structure
 */
struct Circle
{
    /** Robot position specified as (x, y, theta) */
    double center_x;
    double center_y;

    double radius;
}; // Circle

/**
 *  Compute the Circle
 *  Ref: http://paulbourke.net/geometry/circlefrom3/
 */
inline bool calcCircle(RobotState state, Circle target)
{
  bool target_found = false;

  // Segment the laser scan to determine intervals
  double range_diff = 0.0;
  for (int i=1; i<state.laserNumReturns; i++) {
	  // Find discrete gradient
	  range_diff = state.laserRanges[i] - state.laserRanges[i-1];
	  if ( range_diff >= 2.0 ) {
		  printf("range_diff is %5.3f\n", range_diff);
	  }

	  // Check against max range
  };

  // Define three points within each interval
  Point pt1;
  Point pt2;
  Point pt3;
  pt1.x = -1.0; pt1.y = -1.0;
  pt2.x = -1.0; pt2.y = -1.0;
  pt3.x = -1.0; pt3.y = -1.0;

  // Determine the equations of the two lines
  //  Line a passes through Points pt1 and pt2
  //  Line b passes through Points pt2 and pt3
  double m_a = (pt2.y - pt1.y) / (pt2.x - pt1.x);
  double m_b = (pt3.y - pt2.y) / (pt3.x - pt2.x);

  // Compute center coordinates of circle
  target.center_x = ( m_a*m_b*(pt1.y-pt3.y) + m_b*(pt1.x + pt2.x) - m_a*(pt2.x + pt3.x) ) / ( 2*(m_b - m_a) );
  target.center_y = m_a * (target.center_x - pt1.x) + pt1.y;  // using line equation

  // Compute radius of circle
  //  Distance between center and any point
  target.radius = sqrt( pow(pt1.x-target.center_x, 2) + pow(pt1.y-target.center_y, 2) );

  // Determine whether this circle is within the tolerance
  double radius_tol = 0.05;      // 5cm
  if ( abs(target.radius-0.50) <= radius_tol )
    {
      target_found = true;
    }
  else
    {
      target_found = false;
    };

  return target_found;

} // calcCircle





#endif

