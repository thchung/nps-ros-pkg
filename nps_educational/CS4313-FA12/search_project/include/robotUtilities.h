/**
 * File contains utility functions for working with ground robots using
 * the Robot Operating System (ROS)
 */
#ifndef ROBOT_UTILITIES_H
#define ROBOT_UTILITIES_H

#include <cmath>

#include "geometry_msgs/Quaternion.h"

/** Enumeration for state and velocity array access */
enum { X, Y, THETA };

/**
 * Structure definition for a single data object that will contain all robot
 * state information. All of the current fields will be parsed from the
 * nav_msgs/Odometry message (from the "odom" topic).  This struct can be
 * expanded as the amount of state information that we want to maintain
 * increases (e.g., we add sensors to the mix) and we will be able to use
 * multiple callbacks to maintain its data.
 */
struct RobotState
{
    /** Robot position specified as meters (x, y, theta) */
    double pose[3];

    /** Robot velocity specified as meters/second (x-dot, y-dot, theta-dot) */
    double velocity[3];

    // Laser scan data

    /** Minimum angle of the laser scan (radians) */
    float laserMinAngle;

    /** Maximum angle of the laser scan (radians) */
    float laserMaxAngle;

    /** Angular resolution (i.e., radians between each scan element) */
    float laserAngleRes;

    /** Number of individual laser returns in the scan */
    int laserNumReturns;

    /** Minimum range (meters) of the laser */
    float laserMinRange;

    /** Maximum range (meters) of the laser */
    float laserMaxRange;

    /** Individual laser range readings (meters)--array of 1000 simple and efficient */
    float laserRanges[1000];
}; // RobotState


/**
 * Returns the yaw (radians) value equating to a quaternion describing the
 * orientation of the vehicle.  Since the class only equates to ground vehicles
 * we do not care about pitch or roll.  ROS provides quternions in the
 * following format:  (x, y, z, w) where x, y, z and w equate respectively to
 * the q1, q2, q3 and q4 terms from much of the literature
 * @param x
 * @param y
 * @param z
 * @param w
 * @return yaw angle equating to the quaternion
 */
inline double yawFromQuaternion(double x, double y, double z, double w)
{ return atan2((2.0 * (w*z + x*y)), (1.0 - 2.0 * (y*y + z*z))); }


/**
 * Returns the yaw (radians) value equating to a quaternion describing the
 * orientation of the vehicle.  Since the class only equates to ground vehicles
 * we do not care about pitch or roll.
 * @param q:  the quaternion
 * @return yaw angle equating to the quaternion
 */
inline double yawFromQuaternion(const geometry_msgs::Quaternion& q)
{ return atan2((2.0 * (q.w*q.z + q.x*q.y)), (1.0 - 2.0 * (q.y*q.y + q.z*q.z))); }


/**
 * Normalizes an angle so that it is between 0 and 2PI
 * @param theta: angle (radians) to be normalized
 * @return an equivalent angle on a range of [0, 2PI)
 */
inline double normalize2PI(double angle)
{
    while (angle >= (M_PI * 2.0)) angle -= (M_PI * 2.0);
    while (angle < 0.0) angle += (M_PI * 2.0);
    return angle;
}


/**
 * Normalizes an angle so that it is between -PI and PI
 * @param theta: angle (radians) to be normalized
 * @return an equivalent angle on a range of (-PI, PI]
 */
inline double normalizePI(double angle)
{
    while (angle > M_PI) angle -= (M_PI * 2.0);
    while (angle <= -M_PI) angle += (M_PI * 2.0);
    return angle;
}


/**
 * Computes the linear distance between two Cartesian points.
 * @param x1: Cartesian X coordinate of the first point
 * @param y1: Cartesian Y coordinate of the first point
 * @param x2: Cartesian X coordinate of the second point
 * @param y2: Cartesian Y coordinate of the second point
 * @return the linear distance between (x1, y1) and (x2, y2)
 */
inline double distance(double x1, double y1, double x2, double y2)
{  return sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1)); }


/**
 * Computes the linear distance between two Cartesian points.
 * @param pt1: 1x2 array with the Cartesian coordinates of the first point
 * @param pt2: 1x2 array with the Cartesian coordinates of the second point
 * @return the linear distance between (x1, y1) and (x2, y2)
 */
inline double distance(double* pt1, double* pt2)
{  return sqrt((pt2[0]-pt1[0])*(pt2[0]-pt1[0]) + (pt2[1]-pt1[1])*(pt2[1]-pt1[1])); }


/**
 * Computes a heading to go from one point in Cartesian space to another
 * @param p1: Cartesian X coordinate of the first point
 * @param y1: Cartesian Y coordinate of the first point
 * @param x2: Cartesian X coordinate of the second point
 * @param y2: Cartesian Y coordinate of the second point
 * @return the bearing (heading) from (x1, y1) to (x2, y2)
 */
inline double headingTo(double x1, double y1, double x2, double y2)
{ return atan2((y2-y1), (x2-x1)); }


/**
 * Computes a heading to go from one point in Cartesian space to another
 * @param pt1: 1x2 array with the Cartesian coordinates of the first point
 * @param pt2: 1x2 array with the Cartesian coordinates of the second point
 * @return the bearing (heading) from (x1, y1) to (x2, y2)
 */
inline double headingTo(double* pt1, double* pt2)
{ return atan2((pt2[1] - pt1[1]), (pt2[0] - pt1[0])); }


/**
 * Computes a "clamped" value for a value that is between a high and low bound
 * @param val: value for which a saturated value is being computed
 * @param lo: lowest possible value of the clamped value
 * @param hi: highest possible value of the clamped value
 * @return the original value if in the range or the hi or low value if not
 */
inline double saturate(double val, double lo, double hi)
{
    if (val >= hi) return hi;
    if (val <= lo) return lo;
    return val;
} // saturate


/**
 * Computes a "clamped" value for a value that is between an upper bound and
 * and its inverse
 * @param val: value for which a saturated value is being computed
 * @param hi: highest possible value of the clamped value
 * @return the original value if in the range or the hi or low value if not
 */
inline double saturate(double val, double hi)
{
    hi = fabs(hi);
    if (val >= hi) return hi;
    if (val <= -hi) return -hi;
    return val;
} // saturate


/**
 * Returns an integer representing the sign of the parameter.  Will return
 * 1 if the parameter is greater than or equal to 0 and -1 otherwise
 * @param val: value being tested
 * @return 1 if val >= 0, -1 if val < 0
 */
inline int signum(double val)
{ return (val >= 0.0) ? 1 : -1; }


#endif

