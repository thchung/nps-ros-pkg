#!/usr/bin/env python

# import any resources
import csv
import numpy        # NumPy
import time



def brushfireTransform(wf_map, goal_loc):
    '''Apply the brushfire algorithm as a distance transform incorporating the effect of obstacles'''
    wf_dist = wf_map
    
    # Initialize counter
    k = 2;
    
    doneLabeling = False;
    
    while ( not doneLabeling ):
    
        # Check for unlabeled cells
        unlabeled = numpy.nonzero( wf_dist == 0 )
#        print 'Number of cells still unlabeled = ', unlabeled[0].shape[0]

        if ( unlabeled[0].shape[0] == 0 ):       # no unlabeled cells remaining
            # 
            print 'Brushfire labeling complete'
            doneLabeling = True
            
        else:               # still cells unlabeled
            #
            labeled = numpy.nonzero( wf_dist == k )
            
            # Increment the label
            k = k+1
#            print 'Current label: ', k
            
            # For each element in the list of the labeled, update adjacent, unlabeled cells
            for i in range( labeled[0].shape[0] ):
                curr_i = labeled[0][i]; curr_j = labeled[1][i];
#                print 'Inspecting neighbors of cell (', curr_i,',', curr_j,')'  
                
                # for each element, search for adjacent cells not yet visited
                for inc_i in [-1, 0, 1 ]:
                    if (0 <= curr_i+inc_i < wf_dist.shape[0] ):      # within boundaries
                        
                        for inc_j in [-1, 0, 1 ]:
                            if (0 <= curr_j+inc_j < wf_dist.shape[1] ):
                                # Check if already labeled
                                if ( wf_dist[curr_i+inc_i, curr_j+inc_j] == 0):
                                    wf_dist[curr_i+inc_i, curr_j+inc_j] = k
    
#        print wf_dist
    return wf_dist


def distanceTransform(wf_map, goal_loc):
    ''' Compute distance transform from goal location (not accounting for obstacles) '''
    wf_dist = numpy.zeros( wf_map.shape ).astype('int');
    for i in range( wf_map.shape[0] ):
        for j in range( wf_map.shape[1] ):
            if (wf_map[i,j]==0):
                wf_dist[i,j] = int( computeDist(i,j, goal_loc[0], goal_loc[1] ) + 2 )
            else:
                wf_dist[i,j] = int( wf_map[i,j] )
                
#    print 'Distance transform map:', wf_dist

    return wf_dist

    
def computeDist(i1,j1, i2,j2):
    '''Returns the distance between two grid coordinates using 8-point connectivity'''
    return max( abs(i2-i1), abs(j2-j1) )

def coord2cell(i,j,NUM_ROWS,NUM_COLS):
    '''Converts grid coordinate numbers into cell number'''
    cell_num = (i+1) + j*NUM_ROWS
    return cell_num

def wavefront(filename):
    '''Wavefront Algorithm for Coverage'''
    # Load a given map file
    #  - Free space is labeled '0'
    #  - Obstacles are labeled '1'
    #  - Goal location is labeled '2'
    #  Ref: http://stackoverflow.com/questions/4315506/load-csv-into-2d-matrix-with-numpy-for-plotting
    reader = csv.reader(open(filename+'.csv' , "rb"), delimiter=',')
    wf_map = numpy.array(list(reader)).astype('int')

    # Print occupancy map
    print wf_map
    
    # Find the location of the goal cell, which contains a '2'
    goal_loc = numpy.nonzero(wf_map == 2)
#    print goal_loc

    
    # Compute distance transform from goal location
    #  - does not account for obstacles
    #wf_dist = distanceTransform(wf_map, goal_loc)    
    print 'Performing brushfire labeling algorithm...'
    wf_dist = brushfireTransform(wf_map, goal_loc)    


    # Begin wavefront algorithm
    print '------------------------------------------'
    print 'Beginning wavefront algorithm for coverage'

    # Define start location
    #  - index starting at 0
    start_loc = [7,13];
    curr_i = start_loc[0]
    curr_j = start_loc[1]
    print 'Starting location of the robot: (row=%d, col=%d), i.e., cell %d' % \
        (curr_i,curr_j, coord2cell(curr_i,curr_j,wf_map.shape[0], wf_map.shape[1])) 
    # Define variable to store coverage path
    route = [ coord2cell( start_loc[0], start_loc[1], wf_map.shape[0], wf_map.shape[1] ) ];
    
    # Initialize flag
    coverageComplete = False;

    # Compute coverage trajectory
    while (not coverageComplete):
        
        # Compute next location
        # Check all eight locations adjacent to current location that are yet unvisited
        max_cell_val = 0;
        # Check WEST cell
        pred_i = curr_i; pred_j = curr_j-1;
        if (pred_j >= 0 and                             # boundary checking
            coord2cell( pred_i, pred_j, wf_map.shape[0], wf_map.shape[1]) not in route ) :
            if ( wf_dist[pred_i,pred_j] > max_cell_val ):
                max_cell_loc = [pred_i,pred_j];            max_cell_val = wf_dist[pred_i,pred_j ]
        # Check NORTHWEST cell
        pred_i = curr_i-1; pred_j = curr_j-1;
        if (pred_i >= 0 and pred_j >= 0 and             # boundary checking
            coord2cell( pred_i, pred_j, wf_map.shape[0], wf_map.shape[1]) not in route ) :
            if ( wf_dist[pred_i,pred_j] > max_cell_val ):
                max_cell_loc = [pred_i,pred_j];            max_cell_val = wf_dist[pred_i,pred_j ]
        # Check NORTH cell
        pred_i = curr_i-1; pred_j = curr_j;
        if (pred_i >= 0 and                             # boundary checking
            coord2cell( pred_i, pred_j, wf_map.shape[0], wf_map.shape[1]) not in route ) :
            if ( wf_dist[pred_i,pred_j] > max_cell_val ):
                max_cell_loc = [pred_i,pred_j];            max_cell_val = wf_dist[pred_i,pred_j ]
        # Check NORTHEAST cell
        pred_i = curr_i-1; pred_j = curr_j+1;
        if (pred_i >= 0 and pred_j < wf_map.shape[1] and                 # boundary checking
            coord2cell( pred_i, pred_j, wf_map.shape[0], wf_map.shape[1]) not in route ) :
            if ( wf_dist[pred_i,pred_j] > max_cell_val ):
                max_cell_loc = [pred_i,pred_j];            max_cell_val = wf_dist[pred_i,pred_j ]
        # Check EAST cell
        pred_i = curr_i; pred_j = curr_j+1;
        if (pred_j < wf_map.shape[1] and                 # boundary checking
            coord2cell( pred_i, pred_j, wf_map.shape[0], wf_map.shape[1]) not in route ) :
            if ( wf_dist[pred_i,pred_j] > max_cell_val ):
                max_cell_loc = [pred_i,pred_j];            max_cell_val = wf_dist[pred_i,pred_j ]
        # Check SOUTHEAST cell
        pred_i = curr_i+1; pred_j = curr_j+1;
        if (pred_i < wf_map.shape[0] and pred_j < wf_map.shape[1] and                 # boundary checking
            coord2cell( pred_i, pred_j, wf_map.shape[0], wf_map.shape[1]) not in route ) :
            if ( wf_dist[pred_i,pred_j] > max_cell_val ):
                max_cell_loc = [pred_i,pred_j];            max_cell_val = wf_dist[pred_i,pred_j ]
        # Check SOUTH cell
        pred_i = curr_i+1; pred_j = curr_j;
        if (pred_i < wf_map.shape[0] and                 # boundary checking
            coord2cell( pred_i, pred_j, wf_map.shape[0], wf_map.shape[1]) not in route ) :
            if ( wf_dist[pred_i,pred_j] > max_cell_val ):
                max_cell_loc = [pred_i,pred_j];            max_cell_val = wf_dist[pred_i,pred_j ]
        # Check SOUTHWEST cell
        pred_i = curr_i+1; pred_j = curr_j-1;
        if (pred_i < wf_map.shape[0] and pred_j >= 0 and                 # boundary checking
            coord2cell( pred_i, pred_j, wf_map.shape[0], wf_map.shape[1]) not in route ) :
            if ( wf_dist[pred_i,pred_j] > max_cell_val ):
                max_cell_loc = [pred_i,pred_j];            max_cell_val = wf_dist[pred_i,pred_j ]
    
    
        # If all cells have been covered, then max_cell_val will be zero
        # Terminate the coverage process
        if ( max_cell_val == 0 ):
            coverageComplete = True
        else:
            # Add this next cell to the coverage trajectory
            route.append( coord2cell( max_cell_loc[0], max_cell_loc[1], wf_map.shape[0], wf_map.shape[1] ))

            # Update the robot's new location
            curr_i = max_cell_loc[0]
            curr_j = max_cell_loc[1]
            
    
    # Output the resulting coverage trajectory with relevant info
    print ''
    print '------------------------------------------'
    print '------------------------------------------'
    print 'The length of the coverage route is %d' % (len(route))    
    print 'The coverage route is given by: ', route
        
    # Save results to file with suffix
    f = open( filename+'_results'+'.txt', 'w' )
    f.write('The length of the coverage route is %d\n' % (len(route) ) )    
    f.write('The coverage route is given by: \n' )
    
    f.write(str(route))
    f.close()
    

# main file -- if we "import randomwalker" in another file, this code will not execute.
if __name__=="__main__":
    print "Loading 'wavefront' as main"

    # Define the 
    filename = 'mapfile';
    wavefront(filename)
